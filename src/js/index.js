let preTabsLength = 0;

const getTabsLength = ({ termGroups }) => {
  const groups = termGroups.termGroups;
  return Object.keys(groups)
    .map(uid => groups[uid])
    .filter(({ parentUid }) => !parentUid).length;
};

export const middleware = store => next => action => {
  switch (action.type) {
    case 'SESSION_RESIZE': {
      const tabsLength = getTabsLength(store.getState());
      if (preTabsLength <= 1 && tabsLength >= 2) {
        setTimeout(() => {
          store.dispatch({
            type: 'SESSION_RESIZE',
            uid: action.uid,
            cols: action.cols,
            rows: action.rows - 2,
            now: Date.now()
          });
        }, 100);
      }
      preTabsLength = tabsLength;
      break;
    }
    case 'SESSION_PTY_EXIT':
    case 'SESSION_USER_EXIT': {
      preTabsLength = getTabsLength(store.getState());
      break;
    }
  }
  next(action);
};
