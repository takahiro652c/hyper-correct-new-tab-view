# hyper-correct-new-tab-view

[![hyper](https://img.shields.io/badge/hyper-v2.0.0-green.svg)](https://github.com/zeit/hyper/releases/tag/2.0.0)
[![npm](https://img.shields.io/npm/v/hyper-correct-new-tab-view.svg)](https://www.npmjs.com/package/hyper-correct-new-tab-view)
[![npm downloads](https://img.shields.io/npm/dt/hyper-correct-new-tab-view.svg)](https://www.npmjs.com/package/hyper-correct-new-tab-view)
[![gitlab build](https://gitlab.com/takahiro652c/hyper-correct-new-tab-view/badges/master/build.svg)](https://gitlab.com/takahiro652c/hyper-correct-new-tab-view/commits/master)
[![gitlab coverage](https://gitlab.com/takahiro652c/hyper-correct-new-tab-view/badges/master/coverage.svg)](https://gitlab.com/takahiro652c/hyper-correct-new-tab-view/commits/master)

(Hyper plugin) Correct new tab viewpoint.

*Read this in other languages: [日本語](https://gitlab.com/takahiro652c/hyper-correct-new-tab-view/blob/master/README.ja.md)*

## Installation

Open `~/.hyper.js` and add `hyper-correct-new-tab-view` to the list of `plugins`.

```javascript
module.exports = {
  plugins: [
    'hyper-correct-new-tab-view'
  ],
}
```

## License
MIT
