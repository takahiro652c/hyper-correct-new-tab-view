# hyper-correct-new-tab-view

[![hyper](https://img.shields.io/badge/hyper-v2.0.0-green.svg)](https://github.com/zeit/hyper/releases/tag/2.0.0)
[![npm](https://img.shields.io/npm/v/hyper-correct-new-tab-view.svg)](https://www.npmjs.com/package/hyper-correct-new-tab-view)
[![npm downloads](https://img.shields.io/npm/dt/hyper-correct-new-tab-view.svg)](https://www.npmjs.com/package/hyper-correct-new-tab-view)
[![gitlab build](https://gitlab.com/takahiro652c/hyper-correct-new-tab-view/badges/master/build.svg)](https://gitlab.com/takahiro652c/hyper-correct-new-tab-view/commits/master)
[![gitlab coverage](https://gitlab.com/takahiro652c/hyper-correct-new-tab-view/badges/master/coverage.svg)](https://gitlab.com/takahiro652c/hyper-correct-new-tab-view/commits/master)

Hyperで新しいタブを開いたときに、画面下が見切れないようにするプラグイン。

*Read this in other languages: [Engulish](https://gitlab.com/takahiro652c/hyper-correct-new-tab-new/blob/master/README.md)*

## Installation

`~/.hyper.js`ファイルの`plugins`に`hyper-correct-new-tab-new`を追加してください。

```javascript
module.exports = {
  plugins: [
    'hyper-correct-new-tab-new'
  ],
}
```

## License
MIT
